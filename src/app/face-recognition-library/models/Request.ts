
export interface RequestDto{
    numberOfResults: number;
    image: string;    
}

export class Request{

    id: string;
    number: number;
    image: string;

    static convertToRequestDto(request: Request): RequestDto{
        return {
            numberOfResults: request.number,
            image: request.image
        }
    }
}