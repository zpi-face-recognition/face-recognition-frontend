import { Result } from './Result';

export class FaceResult{

    url: string;
    listOfResults: Result[];
}