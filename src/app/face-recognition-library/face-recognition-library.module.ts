import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { MatDialogModule } from "@angular/material/dialog";
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import { FaceRecognitionLibraryRoutingModule } from './face-recognition-library-routing.module';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ResultsComponent } from './components/results/results.component';
import { SharedService } from './service/shared.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {WebcamModule} from 'ngx-webcam';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DragDropDirective } from './directive/drag-drop.directive';
import { HeaderComponent } from './components/header/header.component';


@NgModule({
  declarations: [WelcomeComponent, ResultsComponent, DragDropDirective, HeaderComponent],
  imports: [
    CommonModule,
    FaceRecognitionLibraryRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatStepperModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatListModule,
    MatProgressSpinnerModule,
    WebcamModule
  ],
  exports: [WelcomeComponent, ResultsComponent],
  providers: [SharedService]
})
export class FaceRecognitionLibraryModule { }
