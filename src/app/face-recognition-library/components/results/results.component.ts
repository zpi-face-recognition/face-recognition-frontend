import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../service/shared.service';
import { ResultService } from '../../service/result.service';
import { Subscription } from 'rxjs';
import { Request } from '../../models/Request';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FaceResult } from '../../models/FaceResult';
import { Router } from '@angular/router';

@Component({
  selector: 'cf-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})

export class ResultsComponent implements OnInit {

  isSpinnerDisplayed = false;
  private subscription = new Subscription();
  faceResults: FaceResult[];
  constructor( private sharedService: SharedService, private resultService: ResultService, public router: Router,
     private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getResultsByRequestId();
  }

  getResultsByRequestId(){
    this.isSpinnerDisplayed = true;
    this.subscription.add(this.sharedService.sharedRequest.subscribe(
      (request: Request) => {
        this.getResults(request);
      }
    ))
  }

  getResults(request: Request){
    this.subscription.add(this.resultService.getResultsByRequestId(request.id).subscribe(
      (faceResults: FaceResult[]) => {
        this.faceResults = faceResults;
        this.isSpinnerDisplayed = false;
      }, (e) => {
       this.snackBar.open(e.error.message, 'Close', { duration: 2000 });
       this.router.navigateByUrl('/welcome');
       this.isSpinnerDisplayed = false;
      }))
  }

  onToolbarButtonPress(destination: string){
    if( destination === 'file' || destination === 'camera'){
      this.sharedService.changeMessage(destination);
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}