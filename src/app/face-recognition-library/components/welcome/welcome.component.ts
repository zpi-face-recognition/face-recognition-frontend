import { Component, OnInit, ViewChild } from '@angular/core';
import { Request } from '../../models/Request';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SharedService } from '../../service/shared.service';
import { Subscription, Subject, Observable } from 'rxjs';
import { WebcamUtil, WebcamImage, WebcamInitError } from 'ngx-webcam';
import { DomSanitizer } from '@angular/platform-browser';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'cf-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  @ViewChild('stepper') private myStepper: MatStepper;
  isStepperEnabled = false;
  isSpinnerDisplayed = false;
  firstFormGroup: FormGroup;
  isCompleted = false;
  secondFormGroup: FormGroup;
  file: File;
  subscription = new Subscription();
  pictureSrc;
  isPictureAdded = false;
  isWebcamEnabled = false;
  isDragAndDropEnabled = false;
  allowCameraSwitch = true;
  multipleWebcamsAvailable = false;
  message: string;
  deviceId: string;
  base64image: string;
  videoOptions: MediaTrackConstraints = {
    width: {ideal: 1024},
    height: {ideal: 576}
  };
  errors: WebcamInitError[] = [];
  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  constructor(
    private _formBuilder: FormBuilder,
    private sharedService: SharedService,
    private sanitizer: DomSanitizer,
    ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      uploadCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      numberOfResultsCtrl: ['', Validators.required]
    });

    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });

      this.sharedService.currentMessage.subscribe(message => this.message = message)
      if(this.message=== 'file' || this.message ==='camera'){
        this.onToolbarButtonPress(this.message);
      }
  }

  start() {
    this.isStepperEnabled = true;
    this.isDragAndDropEnabled = false;
    this.isWebcamEnabled = false;
  }

  triggerSnapshot(): void {
    this.trigger.next();
  }
  
  initWebcam(): void {
    this.isWebcamEnabled = true;
    this.isDragAndDropEnabled = false;
    this.isPictureAdded = false;
    this.isCompleted = false;
  }

  initDragAndDrop(): void {
    this.isDragAndDropEnabled = true;
    this.isWebcamEnabled = false;
    this.isPictureAdded = false;
    this.isCompleted = false;
  }

  handleImage(webcamImage: WebcamImage): void {
    this.base64image = webcamImage.imageAsBase64;
    this.convertToJPG(webcamImage.imageAsBase64);
  }

  uploadFile(fileList) {
    this.isDragAndDropEnabled = false;
    var reader = new FileReader();
    reader.readAsDataURL(fileList[0]);

    reader.onload = (e) => {
      this.base64image = reader.result.toString().split("base64,")[1];
      this.convertToJPG(reader.result.toString().split("base64,")[1]);
    };
    
  }

  convertToJPG(imageAsBase64){
    const byteString = window.atob(imageAsBase64);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }    
    let blob = new Blob([int8Array], {type: 'image/jpg'});
    
    this.isWebcamEnabled = false;
    this.addFile(new File([blob], 'faceImage.jpg'));
  }

  addFile(file) {
    var reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = (e) => {
      this.pictureSrc = reader.result;
      this.pictureSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.pictureSrc);
    };

    this.file = file;
    this.isCompleted = true;
    this.isPictureAdded = true;
  }

  uploadImageAndStart() {
    this.isSpinnerDisplayed = true;
        const formData = new FormData();
        formData.append('image', this.file);
          let request: Request = {
            id: null,
            number: this.secondFormGroup.controls['numberOfResultsCtrl'].value,
            image: this.base64image,
          }
          this.sharedService.addRequest(request);
  }

  showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }
  
  get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  onToolbarButtonPress(source : string): void {
    this.isStepperEnabled = true;
    this.isPictureAdded = false;
    this.isCompleted = false;

    if(this.myStepper){
      this.myStepper.reset();
    }
    if(source === "file"){
      this.isDragAndDropEnabled = true;
      this.isWebcamEnabled = false;
    }
    if(source === "camera"){
      this.isDragAndDropEnabled = false;
      this.isWebcamEnabled = true;
    }
    if(source === "welcome"){
      this.isStepperEnabled = false;
    }
    this.sharedService.changeMessage('');
  }

}
