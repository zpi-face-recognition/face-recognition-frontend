import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Endpoints } from '../enums/Endpoints';
import { Request } from '../models/Request';
import { FaceResult } from '../models/FaceResult';

@Injectable({
  providedIn: 'root'
})
export class ResultService {
  private API: string;

  constructor(private http: HttpClient) {
    this.API = `${Endpoints.API}`;
  }

  public getResultsByRequestId(requestId: string): Observable<FaceResult[]> {
    return this.http.get<FaceResult[]>(this.API + `${Endpoints.RESULTS}/${requestId}`).pipe(
      map((data: FaceResult[]) => data));
  }

  public addRequest(request: Request): Observable<Request>{
    const requestDto = Request.convertToRequestDto(request);
    return this.http.post<Request>(this.API + `${Endpoints.RESULTS}`, requestDto).pipe(
      map((data: Request) => data));
  }
}
