import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Request } from '../models/Request';
import { ResultService } from './result.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SharedService {

  private requestData = new BehaviorSubject<Object>(1);
  public sharedRequest = this.requestData.asObservable();
  private subscription = new Subscription();
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  constructor(private resultService: ResultService, public router: Router, private snackBar: MatSnackBar) { }

  public addRequest(request: Request){
    this.subscription.add(this.resultService.addRequest(request).subscribe(
      (request: Request) =>{
      this.requestData.next(request);
      this.router.navigateByUrl('/results');
      this.snackBar.open('Face is being analyzed!', 'Close', {duration: 2000});
      }, (e) => {
        this.snackBar.open(e.error.message, 'Close', { duration: 2000 });
       }
    ))
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  
}