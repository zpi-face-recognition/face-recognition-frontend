import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './face-recognition-library/components/welcome/welcome.component';
import { ResultsComponent } from './face-recognition-library/components/results/results.component';


const routes: Routes = [
  {path: '', redirectTo: '/welcome', pathMatch: 'full'},
   {path: 'welcome', component:WelcomeComponent},
   {path: 'results', component: ResultsComponent}
 ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
