import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FaceRecognitionLibraryModule } from './face-recognition-library/face-recognition-library.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FaceRecognitionLibraryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
